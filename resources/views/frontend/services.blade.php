@extends('frontend.includes.app')
@section('title','Services')
@section('content')
    <!--====== BREADCRUMB SECTION START ======-->
    <section class="breadcrumb-section" style="background-image: url({{ asset('assets/img/breadcrumb.jpg') }});">
        <div class="container">
            <div class="breadcrumb-text">
                <h1>web development</h1>
                <p>Your Partner for Software Innovation</p>
            </div>
            <ul class="breadcrumb-nav">
                <li><a href="#">Home</a></li>
                <li>our services</li>
            </ul>
            <span class="btg-text">TECHBUCKS</span>
        </div>
        <div class="breadcrumb-shape">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                <path d="M500,97C126.7,96.3,0.8,19.8,0,0v100l1000,0V1C1000,19.4,873.3,97.8,500,97z">
                </path>
            </svg>
        </div>
    </section>
    <!--====== BREADCRUMB SECTION END ======-->
    <!--====== FEATURE SECTION START ======-->
    <section class="features-boxes-two pt-180 pb-120">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <div class="features-boxes-loop">
                        <!-- Loop start -->
                        <div class="row fetaure-masonary">
                            <div class="col-md-6 col-sm-6">
                                <div class="feature-box-two text-center">
                                    <div class="icon text-center">
                                        <i class="fas fa-paper-plane"></i>
                                    </div>
                                    <h4>our vision</h4>
                                    <p>Our mission is streamlined with our vision. Our commitment is to provide accurate
                                        results, a diverse workplace and promises delivered on time. We keep in mind all
                                        the key factors required to completely digitalize our company in a way to make
                                        sure we’re one of the leading technology companies
{{--                                        with the best trained staff--}}
{{--                                        at a global level.--}}
                                    </p>

                                    <span class="count">01</span>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="feature-box-two mt-30 text-center">
                                    <div class="icon text-center">
                                        <i class="fas fa-globe"></i>
                                    </div>
                                    <h4>our mission</h4>
                                    <p>Aspiring to grow and come up with innovative software solutions and digital
                                        transformation techniques to help our clients achieve their targets is what we
                                        aim for. We inspire our team to take an active part in futuristic platforms
                                    </p>
                                    <span class="count">02</span>
                                </div>
                            </div>
{{--                            <div class="col-md-6 col-sm-6">--}}
{{--                                <div class="feature-box-two mt-30 text-center">--}}
{{--                                    <div class="icon text-center">--}}
{{--                                        <i class="fas fa-users"></i>--}}
{{--                                    </div>--}}
{{--                                    <h4>our approch</h4>--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod--}}
{{--                                        tempor--}}
{{--                                    </p>--}}
{{--                                    <span class="count">03</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-md-6 col-sm-6">--}}
{{--                                <div class="feature-box-two mt-30 text-center">--}}
{{--                                    <div class="icon text-center">--}}
{{--                                        <i class="fas fa-cogs"></i>--}}
{{--                                    </div>--}}
{{--                                    <h4>our strategy</h4>--}}
{{--                                    <p>Lorem ipsum dolor sit amet, consectet ur adipisicing elit, sed do eiusmod--}}
{{--                                        tempor--}}
{{--                                    </p>--}}
{{--                                    <span class="count">04</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-10">
                    <div class="feature-text pl-50">
                        <div class="section-title left-border mb-40">
                            <span class="title-tag">COLLABRATION WITH US</span>
                            <h2>Dedicated IT Solutions with 4+ Years Experience.</h2>
                        </div>
                        <p>Over the years, a wide range of developments and innovations in the global IT arena have led
                            to many new IT-enabled devices and services being produced. Moreover, there is need for IT
                            today, not just in urban areas but rural regions as well.</p>

                        <div class="row">
                            <div class="col-md-12" style="display: inline-flex;">
                                <div class="col-md-4">
                                    <img src="{{ asset('assets/img/services/icon-1.png') }}" alt="Icon">
                                    <h6>WEBSITE DEVELOPMENT</h6>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{ asset('assets/img/services/icon-2.png') }}" alt="Icon">
                                    <h6>MOBILE APP DEVELOPMENT</h6>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{ asset('assets/img/services/icon-3.png') }}" alt="Icon">
                                    <h6>E-COMMERCE</h6>
                                </div>
                            </div>
                            <div class="col-md-12" style="display: inline-flex;">
                                <div class="col-md-4">
                                    <img src="{{ asset('assets/img/services/icon-4.png') }}" alt="Icon">
                                    <h6>SOFTWARE DEVELOPMENT</h6>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{ asset('assets/img/services/icon-5.png') }}" alt="Icon">
                                    <h6>UI/UX DESIGN</h6>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{ asset('assets/img/services/icon-6.png') }}" alt="Icon">
                                    <h6>TECHNICAL SUPPORT</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== FEATURE SECTION END ======-->
    <!--====== FRAMEWORK PART START ======-->
    @include('frontend.partials.framework')
    <!--====== FRAMEWORK PART end ======-->
    <!--====== TESTIMONIALS & clinet SECTION ======-->
    @include('frontend.partials.testimonials')
    <!--====== TESTIMONIALS & BRAND SECTION END ======-->
@endsection
