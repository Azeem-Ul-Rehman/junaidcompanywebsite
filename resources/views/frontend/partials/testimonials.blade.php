<section class="testimonial-section pt-120 pb-120">
    <div class="container">
        <!-- testimonial Content Slider-->
        <div class="testimonial-slider mb-45">
            <div class="single-testimonial">
                <div class="author-img">
                    <img src="{{ asset('assets/img/test-author.png') }}" alt="Hostan From Canada">
                </div>
                <div class="desc">
                    <div class="rateing mb-20">
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        {{--                            <a href="#"><i class="fal fa-star"></i></a>--}}
                    </div>
                    <h2>“ Excellent and did the task fast. added everything in the required features and did extra
                        in making sure everything was set up properly. Very patient and a great communicator ”</h2>
                </div>
            </div>
            <div class="single-testimonial">
                <div class="author-img">
                    <img src="{{ asset('assets/img/test-author.png') }}" alt="Singapore Danclats">
                </div>
                <div class="desc">
                    <div class="rateing mb-20">
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                    </div>
                    <h2>“ Excellent service, TECHBUCKS produced all the required source code, met all my technical
                        requirements and designed a great website too! Delivery was well within the established
                        timeframe, and all for a very reasonable price! Kudos! ”</h2>
                </div>
            </div>
            <div class="single-testimonial">
                <div class="author-img">
                    <img src="{{ asset('assets/img/test-author.png') }}" alt="France HANSTHECAT">
                </div>
                <div class="desc">
                    <div class="rateing mb-20">
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                        <a href="#"><i class="fas fa-star"></i></a>
                    </div>
                    <h2>“ TECHBUCKS always delivers the best possible result. Perfect communication, extreme
                        availability. He’s so professional and responsive - I sincerely recommend him! ”</h2>
                </div>
            </div>
        </div>
        <!-- Author Info Slider -->
        <div class="row no-gutters justify-content-center">
            <div class="col-lg-9">
                <div class="testimonial-author-slider">
                    <div class="single-slider">
                        <h4>Hostan</h4>
                        <span>From Canada</span>
                    </div>
                    <div class="single-slider">
                        <h4>Danclats</h4>
                        <span>From Singapore</span>
                    </div>
                    <div class="single-slider">
                        <h4>HANSTHECAT</h4>
                        <span>From France</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
