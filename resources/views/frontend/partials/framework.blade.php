<section class="framework-section padding-bottom-extra">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-6 col-md-10">
                <div class="framework-img">
                    <img src="{{ asset('assets/img/framework.jpg') }}" alt="Image">
                </div>
            </div>
            <div class="col-lg-6 col-md-10">
                <div class="framework-text pl-20">
                    <div class="section-title left-border mb-40">
                        <span class="title-tag">framework</span>
                        <h2> we are using best quality framwork for you. </h2>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade" id="nodejs" role="tabpanel">
                            <p>
                                Over the years, a wide range of developments and innovations in the global IT arena
                                have led to many new IT-enabled devices and services being produced. Moreover, there
                                is need for IT today, not just in urban areas but rural regions as well.
                            </p>
                        </div>
                        <div class="tab-pane fade show active" id="laravel" role="tabpanel">
                            <p>
                                Over the years, a wide range of developments and innovations in the global IT arena
                                have led to many new IT-enabled devices and services being produced. Moreover, there
                                is need for IT today, not just in urban areas but rural regions as well.
                            </p>
                        </div>
                        <div class="tab-pane fade" id="html" role="tabpanel">
                            <p>
                                Over the years, a wide range of developments and innovations in the global IT arena
                                have led to many new IT-enabled devices and services being produced. Moreover, there
                                is need for IT today, not just in urban areas but rural regions as well.
                            </p>
                        </div>
                        <div class="tab-pane fade" id="wordpress" role="tabpanel">
                            <p>
                                Over the years, a wide range of developments and innovations in the global IT arena
                                have led to many new IT-enabled devices and services being produced. Moreover, there
                                is need for IT today, not just in urban areas but rural regions as well.
                            </p>
                        </div>
                    </div>
                    <ul class="framework-list nav nav-pills mt-25" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="pills-python-tab" data-toggle="pill" href="#nodejs">
                                    <span class="icon">
                                        <i class="fab fa-node-js"></i>
                                    </span>
                                Node
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-wordpress-tab" data-toggle="pill"
                               href="#laravel">
                                    <span class="icon">
                                        <i class="fab fa-laravel"></i>
                                    </span>
                                Laravel
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-html-tab" data-toggle="pill" href="#html">
                                    <span class="icon">
                                        <i class="fab fa-html5"></i>
                                    </span>
                                HTML5
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-java-tab" data-toggle="pill" href="#wordpress" role="tab">
                                    <span class="icon">
                                        <i class="fab fa-wordpress"></i>
                                    </span>
                                Wordpress
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
