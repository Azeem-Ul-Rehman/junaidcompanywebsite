<header class="header-absolute sticky-header">
    <div class="container-fluid custom-container-one">
        <div class="header-top-area">
            <div class="row align-items-center">
                <div class="col-md-6 col-sm-7">
                    <ul class="contact-list">
                        <li><a href="#">info@thetechbucks.com</a></li>
                        <li><a href="#">+92334-4489986</a></li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-5">
                    <ul class="social-link">
                        {{--                        <li><a href="#">fb</a></li>--}}
                        {{--                        <li><a href="#">tw</a></li>--}}
                        {{--                        <li><a href="#">be</a></li>--}}
                        {{--                        <li><a href="#">yu</a></li>--}}
                        {{--                        <li><a href="#">li</a></li>--}}
                    </ul>
                </div>
            </div>
        </div>
        <div class="mainmenu-area">
            <div class="d-flex align-items-center justify-content-between">
                <nav class="main-menu">
                    <div class="logo">
                        <a href="{{ route('index') }}"><img src="{{ asset('assets/img/logo.png') }}" alt="TECHBUCKS"
                                                            style="width: 200px !important;"></a>
                    </div>
                    <div class="menu-items">
                        <ul>
                            <li class="active">
                                <a href="{{ route('index') }}">Home</a>
                            </li>
                            <li>
                                <a href="{{ route('about') }}">About</a>
                            </li>
                            <li>
                                <a href="{{ route('services') }}">Services</a>
                                {{--                                <ul class="submenu">--}}
                                {{--                                    <li><a href="service-details.html">Service Details</a></li>--}}
                                {{--                                </ul>--}}
                            </li>
                            <li>
                                <a href="#portfolio">Portfolio</a>
                            </li>
                            <li>
                                <a href="{{ route('contact-us') }}">Contact</a>
                            </li>
                        </ul>
                    </div>
                </nav>
                <div class="main-right">
                    <a href="#" class="main-btn btn-borderd">
                        Get A Quote
                    </a>
                </div>
            </div>
        </div>
        <!-- Mobile Menu -->
        <div class="row">
            <div class="col-12">
                <div class="mobile-menu"></div>
            </div>
        </div>
    </div>
</header>
