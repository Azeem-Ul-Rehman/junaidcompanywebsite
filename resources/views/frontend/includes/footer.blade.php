<footer>
    <div class="footer-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-4">
                    <div class="logo text-center text-sm-left">
                        <a href="{{ route('index') }}">
                            <img src="{{ asset('assets/img/logo.png') }}" style="width: 50%" alt="TECHBUCKS">
                        </a>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="social-icon text-center text-sm-right">
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-behance"></i></a>
                        <a href="https://www.linkedin.com/company/techbucks" target="_blank"><i class="fab fa-linkedin"></i></a>
                        <a href="#"><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- widgets -->
    <div class="footer-widget-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widget nav-widget d-flex justify-content-start">
                        <div>
                            <h5 class="widget-title">Company</h5>
                            <ul>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Leadership</a></li>
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Legal</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="widget nav-widget d-flex justify-content-start justify-content-lg-center">
                        <div>
                            <h5 class="widget-title">Solutions</h5>
                            <ul>
                                <li><a href="#">Web & Mobile Apps</a></li>
                                <li><a href="#">Website Hosting</a></li>
                                <li><a href="#">Digital Marketing</a></li>
                                <li><a href="#">UI/UX Designs</a></li>
                                <li><a href="#">Video Animations</a></li>
                                <li><a href="#">Business Solutions</a></li>
                                <li><a href="#">Open Source</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="widget recent-post">
                        <div>
                            <h5 class="widget-title">news feeds</h5>
                            <div class="post-loop">
                                <div class="post">
                                    <div class="post-img">
                                        <img src="{{ asset('assets/img/recent-post-wid/01.jpg') }}" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="time"><i class="fal fa-calendar-alt"></i> 14th May 2020</span>
                                        <h5><a href="#">Managing Partner along with Senior
                                                Counsels.</a></h5>
                                    </div>
                                </div>
                                <div class="post">
                                    <div class="post-img">
                                        <img src="{{ asset('assets/img/recent-post-wid/02.jpg') }}" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="time"><i class="fal fa-calendar-alt"></i> 14th May 2020</span>
                                        <h5><a href="#">Managing Partner along with Senior
                                                Counsels.</a></h5>
                                    </div>
                                </div>
                                <div class="post">
                                    <div class="post-img">
                                        <img src="{{ asset('assets/img/recent-post-wid/03.jpg') }}" alt="Image">
                                    </div>
                                    <div class="post-desc">
                                        <span class="time"><i class="fal fa-calendar-alt"></i> 14th May 2020</span>
                                        <h5><a href="#">Managing Partner along with Senior
                                                Counsels.</a></h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- copyright -->
    <div class="copy-right-area">
        <div class="container">
            <div class="copyrigt-text d-sm-flex justify-content-between">
                <p>Copyright by@<a href="#">TECHBUCKS</a> - 2021</p>
                <p>Design by@<a href="#">TECHBUCKS</a> - 2021</p>
            </div>
        </div>
    </div>
</footer>
