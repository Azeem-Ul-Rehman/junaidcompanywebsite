<!DOCTYPE html>
<html lang="en">
<head>
    <!--====== Required meta tags ======-->
    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge"/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <!--====== Title ======-->
    <title>TECHBUCKS || @yield('title')</title>
    <!--====== Favicon Icon ======-->
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.html') }}" type="img/png"/>
    <!--====== Animate Css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.min.css') }}">
    <!--====== Bootstrap css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}"/>
    <!--====== Fontawesome css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}"/>
    <!--====== PE Icon 7 ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/pe-icon-7-stroke.css') }}"/>
    <!--====== Magnific Popup css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}"/>
    <!--====== Owl Carousel css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}"/>
    <!--====== Mean Menu ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/meanmenu.min.css') }}"/>
    <!--====== Default css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/default.css') }}"/>
    <!--====== Style css ======-->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}"/>
</head>
<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->
<!--====== PRELOADER ======-->
<div id="preloader">
    <div>
        <div></div>
    </div>
</div>
<!--====== HEADER START ======-->
@include('frontend.includes.header')
<!--====== HEADER END ======-->
@yield('content')

<!--====== GO TO TOP PART START ======-->
<div class="go-top-area">
    <div class="go-top-wrap">
        <div class="go-top-btn-wrap">
            <div class="go-top go-top-btn">
                <i class="fal fa-angle-double-up"></i>
                <i class="fal fa-angle-double-up"></i>
            </div>
        </div>
    </div>
</div>
<!--====== GO TO TOP PART ENDS ======-->
<!--====== FOOTER PART START ======-->
@include('frontend.includes.footer')
<!--====== FOOTER PART END ======-->
<!--====== jquery js ======-->
<script src="{{ asset('assets/js/vendor/modernizr-3.6.0.min.js') }}"></script>
<script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
<!--====== Bootstrap js ======-->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<!--====== Slick js ======-->
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<!--====== Isotope js ======-->
<script src="{{ asset('assets/js/isotope.pkgd.min.js') }}"></script>
<!--====== Magnific Popup js ======-->
<script src="{{ asset('assets/js/jquery.magnific-popup.min.js') }}"></script>
<!--====== inview js ======-->
<script src="{{ asset('assets/js/jquery.inview.min.js') }}"></script>
<!--====== counterup js ======-->
<script src="{{ asset('assets/js/jquery.countTo.js') }}"></script>
<!--====== easypiechart js ======-->
<script src="{{ asset('assets/js/jquery.easypiechart.js') }}"></script>
<!--====== Mean Menu ======-->
<script src="{{ asset('assets/js/jquery.meanmenu.min.js') }}"></script>
<!--====== Main js ======-->
<script src="{{ asset('assets/js/main.js') }}"></script>
</body>
</html>
