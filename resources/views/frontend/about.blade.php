@extends('frontend.includes.app')
@section('title','About Us')
@section('content')
    <!--====== BREADCRUMB SECTION START ======-->
    <section class="breadcrumb-section" style="background-image: url({{ asset('assets/img/breadcrumb.jpg') }});">
        <div class="container">
            <div class="breadcrumb-text">
                <h1>About Us</h1>
                <p>Your Partner for Software Innovation</p>
            </div>
            <ul class="breadcrumb-nav">
                <li><a href="#">Home</a></li>
                <li>About Us</li>
            </ul>
            <span class="btg-text">TECHBUCKS</span>
        </div>
        <div class="breadcrumb-shape">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                <path d="M500,97C126.7,96.3,0.8,19.8,0,0v100l1000,0V1C1000,19.4,873.3,97.8,500,97z">
                </path>
            </svg>
        </div>
    </section>
    <!--====== BREADCRUMB SECTION END ======-->
    <!--====== ABOUT SECTION START ======-->
    <section class="about-section about-style-three pt-120 pb-120">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <div class="about-tile-gallery-two">
                        <img src="{{ asset('assets/img/tile-gallery/04.jpg') }}" alt="Image" class="image-one">
                        <img src="{{ asset('assets/img/tile-gallery/05.jpg') }}" alt="Image" class="image-two">
                    </div>
                </div>
                <div class="col-lg-6 col-md-10">
                    <div class="about-text pl-30">
                        <div class="section-title left-border mb-40">
                            <span class="title-tag">About us</span>
                            <h2>We are a Fearless Powerhouse of Ideas!</h2>
                        </div>
                        <p>
                            Over the years, TECHBUCKS is the one-stop-shop to get web design, web development, app
                            development, and e-commerce services. Result-driven techniques to expand
                            your digital horizon.
                        </p>
                        <div class="about-extra">
                            Over the years, a wide range of developments and innovations in the global IT arena have
                            led to many new IT-enabled devices and services being produced.
                            <div class="experience-tag">
                                <img src="{{ asset('assets/img/experience-tag.png') }}" alt="Image">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== ABOUT SECTION END ======-->
    <!--====== SERVICES SECTION START ======-->
    <section class="services-secton services-secton-three pt-120 pb-120">
        <div class="container">
            <!-- Section Title -->
            <div class="row align-items-center justify-content-between">
                <div class="col-lg-5 col-md-8 col-sm-7">
                    <div class="section-title left-border">
                        <span class="title-tag">our services</span>
                        <h2>see what we do here with good passions</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4 col-sm-5 d-none d-sm-block">
                    <div class="service-page-link- text-right">
                        <a href="#" class="main-btn btn-filled">learn more</a>
                    </div>
                </div>
            </div>
            <div class="services-loop mt-50">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-service-two white-bg">
                            <div class="top-icon">
                                <i class="fal fa-long-arrow-right"></i>
                            </div>
                            <h4>Website Development</h4>
                            <p>A website is your online identity. It should be made well to improve your online
                                identity. Get your website in industry-leading language or technologies that include
                                Laravel, React, Vuejs, Nodejs, WordPress, and more
                            </p>
                            <div class="bottom-icon">
                                <i class="pe-7s-diamond"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-service-two white-bg">
                            <div class="top-icon">
                                <i class="fal fa-long-arrow-right"></i>
                            </div>
                            <h4>Mobile App Development</h4>
                            <p>Get mobile-friendly apps as per your business needs to stay ahead of the curve. We are
                                here to offer you the best Android, iOS, or Hybrid apps to make your business and
                                interaction with customers easy. </p>
                            <div class="bottom-icon">
                                <i class="pe-7s-arc"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-service-two white-bg">
                            <div class="top-icon">
                                <i class="fal fa-long-arrow-right"></i>
                            </div>
                            <h4>E-commerce</h4>
                            <p>Want to sell your products to millions across the globe? E-commerce is the answer for
                                you! Time to scale up your e-commerce business for increasing your profits to many folds
                                with our best business-driving strategies.</p>
                            <div class="bottom-icon">
                                <i class="pe-7s-refresh-2"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-service-two white-bg">
                            <div class="top-icon">
                                <i class="fal fa-long-arrow-right"></i>
                            </div>
                            <h4>Software Development</h4>
                            <p>Want to sell your products to millions across the globe? E-commerce is the answer for
                                you! Time to scale up your e-commerce business for increasing your profits to many folds
                                with our best business-driving strategies.</p>
                            <div class="bottom-icon">
                                <i class="pe-7s-world"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-service-two white-bg">
                            <div class="top-icon">
                                <i class="fal fa-long-arrow-right"></i>
                            </div>
                            <h4>UI/UX Design</h4>
                            <p>UI/UX consultation and development services to design software products that win the
                                hearts of your customers.We will draw your dreams digitally and according to your
                                expectations. </p>
                            <div class="bottom-icon">
                                <i class="pe-7s-world"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6">
                        <div class="single-service-two white-bg">
                            <div class="top-icon">
                                <i class="fal fa-long-arrow-right"></i>
                            </div>
                            <h4>Technical Support</h4>
                            <p>Get mobile-friendly apps as per your business needs to stay ahead of the curve. We are
                                here to offer you the best Android, iOS, or Hybrid apps to make your business and
                                interaction with customers easy. </p>
                            <div class="bottom-icon">
                                <i class="pe-7s-world"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== SERVICES SECTION END ======-->
    <!--====== FRAMEWORK PART START ======-->
    @include('frontend.partials.framework')
    <!--====== FRAMEWORK PART end ======-->
    <!--====== CONTERUP PART START ======-->
    <section class="counter-section mt-negative">
        <div class="container">
            <div class="counter-inner">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-6">
                        <div class="counter-box">
                            <h1><span class="counter">100</span><span>+</span></h1>
                            <p class="title">Project Done</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-6">
                        <div class="counter-box">
                            <h1><span class="counter">20</span><span>+</span></h1>
                            <p class="title">Active Client</p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-6">
                        <div class="counter-box">
                            <h1><span class="counter">15</span><span>+</span></h1>
                            <p class="title">Drink Coffee</p>
                        </div>
                    </div>

                </div>
                <span class="big-text">
                    TECHBUCKS
                </span>
            </div>
        </div>
    </section>
    <!--====== CONTERUP PART END ======-->
@endsection
