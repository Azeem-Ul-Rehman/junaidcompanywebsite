@extends('layouts.master')
@section('title','General')

@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ __('General') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.general.store') }}"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">
                                        <div class="col-md-12">


                                            <label for="title"><strong>Title</strong></label>

                                            <textarea name="title" id="title" rows="10" cols="144"
                                                      required>{{ ($general) ? $general->title : '' }}</textarea>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">


                                            <label for="description"><strong>Description</strong></label>

                                            <textarea name="description" id="description" rows="10" cols="80"
                                                      required>{{ ($general) ? $general->description : '' }}</textarea>

                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">


                                            <label for="shipping_details"><strong>Shipping Details</strong></label>

                                            <textarea name="shipping_details" id="shipping_details" rows="10" cols="80"
                                                      required>{{ ($general) ? $general->shipping_details : '' }}</textarea>

                                            @error('shipping_details')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">


                                            <label for="payment_details"><strong>Payment Details</strong></label>

                                            <textarea name="payment_details" id="payment_details" rows="10" cols="80"
                                                      required>{{ ($general) ? $general->payment_details : '' }}</textarea>

                                            @error('payment_details')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">


                                            <label for="return_policy"><strong>Return Policy</strong></label>

                                            <textarea name="return_policy" id="return_policy" rows="10" cols="80"
                                                      required>{{ ($general) ? $general->return_policy : '' }}</textarea>

                                            @error('return_policy')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-12">


                                            <label for="additional_information"><strong>Additional Information</strong></label>

                                            <textarea name="additional_information" id="additional_information"
                                                      rows="10" cols="80"
                                                      required>{{ ($general) ? $general->additional_information : '' }}</textarea>

                                            @error('additional_information')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection
