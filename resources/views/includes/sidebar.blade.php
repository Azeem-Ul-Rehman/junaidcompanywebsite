<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
        class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item  "
                aria-haspopup="true"><a href="{{ route('index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-home"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Home</span>
											</span></span></a></li>

            <li class="m-menu__item {{ (request()->is('admin/galleries')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.galleries.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-photo-camera"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Gallery</span>
											</span></span></a></li>

            <li class="m-menu__item {{ (request()->is('admin/features')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.features.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-box"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Features</span>
											</span></span></a></li>

            <li class="m-menu__item {{ (request()->is('admin/general')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.general.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-information"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">General</span>
											</span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('admin/settings') || request()->is('admin/settings/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('admin.settings.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-settings"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Settings</span>
											</span></span></a></li>


        </ul>

    </div>

    <!-- END: Aside Menu -->
</div>
