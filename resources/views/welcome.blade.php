@extends('frontend.includes.app')
@section('title','Home')
@section('content')

    <!--====== BANNER SECTION START ======-->
    <section class="banner-section" style="background-image: url({{ asset('assets/img/banner/01.jpg') }});">
        <div class="slider-active" id="bannerSliderOne">
            <div class="single-banner">
                <div class="container container-custom">
                    <div class="row extra-left">
                        <div class="col-lg-8">
                            <div class="banner-text">
                                <h1 data-animation="fadeInUp" data-delay=".7s" style="animation-delay: 0.7s;">We Develop
                                    what you need</h1>
                                <p data-animation="fadeInUp" data-delay=".9s">Bring your ideas to life with us</p>
                                <div class="btn-wrap" data-animation="fadeIn" data-delay="1.5s">
                                    <a href="#" class="main-btn btn-filled">Get Started Now</a>
                                    <a href="#" class="main-btn btn-borderd">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-banner">
                <div class="container container-custom">
                    <div class="row extra-left">
                        <div class="col-lg-8">
                            <div class="banner-text">
                                <h1 data-animation="fadeInUp" data-delay=".7s" style="animation-delay: 0.7s;">Adaptive
                                    <small>to your business, your growth & the future</small></h1>
                                <p data-animation="fadeInUp" data-delay=".9s">Providing reliable IT services
                                    globally</p>
                                <div class="btn-wrap" data-animation="fadeIn" data-delay="1.5s">
                                    <a href="#" class="main-btn btn-filled">Get Started Now</a>
                                    <a href="#" class="main-btn btn-borderd">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="single-banner">
                <div class="container container-custom">
                    <div class="row extra-left">
                        <div class="col-lg-8">
                            <div class="banner-text">
                                <h1 data-animation="fadeInUp" data-delay=".7s" style="animation-delay: 0.7s;">Hub of IT
                                    Services</h1>
                                <p data-animation="fadeInUp" data-delay=".9s">Convey your message digitally</p>
                                <div class="btn-wrap" data-animation="fadeIn" data-delay="1.5s">
                                    <a href="#" class="main-btn btn-filled">Get Started Now</a>
                                    <a href="#" class="main-btn btn-borderd">Learn More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner-shape-one"></div>
        <div class="banner-shape-two"></div>
    </section>
    <!--====== BANNER SECTION END ======-->
    <!--====== ABOUT SECTION START ======-->
    <section class="about-section pt-120 pb-120">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-6 col-md-10">
                    <div class="about-tile-gallery">
                        <img src="{{ asset('assets/img/tile-gallery/01.jpg') }}" alt="Image" class="image-one">
                        <img src="{{ asset('assets/img/tile-gallery/02.jpg') }}" alt="Image" class="image-two">
                        <img src="{{ asset('assets/img/tile-gallery/03.jpg') }}" alt="Image" class="image-three">
                        <img src="{{ asset('assets/img/tile-gallery/icon.png') }}" alt="icon" class="icon">
                        <img src="{{ asset('assets/img/tile-gallery/icon-2.png') }}" alt="icon" class="zero-icon">
                    </div>
                </div>
                <div class="col-lg-6 col-md-10">
                    <div class="about-text pl-25">
                        <div class="section-title mb-40 left-border">
                            <span class="title-tag">About us</span>
                            <h2>We are a Fearless Powerhouse of Ideas!</h2>
                        </div>
                        <p>
                            Over the years, TECHBUCKS is the one-stop-shop to get web design, web development, app
                            development, and e-commerce services. Result-driven techniques to expand
                            your digital horizon.
                        </p>
                        <div class="about-features mt-50">
                            <div class="sngle-features">
                                <div class="chart" data-percent="75">
                                    <span class="icon"><i class="fas fa-award"></i></span>
                                </div>
                                <div class="counter-box">
                                    <span class="timer">4</span><span>+</span>
                                </div>
                                <div class="desc">
                                    <h4>Years of Experience</h4>
                                    <p>As a result, most of us need to know how to use computers. Our knowledge of
                                        computers.</p>
                                </div>
                            </div>
                            <div class="sngle-features">
                                <div class="chart" data-percent="85">
                                    <span class="icon"><i class="fas fa-globe"></i></span>
                                </div>
                                <div class="counter-box">
                                    <span class="timer">100</span><span>+</span>
                                </div>
                                <div class="desc">
                                    <h4>Project Done together</h4>
                                    <p>As a result, most of us need to know how to use computers. Our knowledge of
                                        computers.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== ABOUT SECTION END ======-->
    <!--====== SERVICES SECTION START ======-->
    <section class="services-secton pt-120 pb-200">
        <div class="container">
            <div class="section-title mb-80 text-center both-border">
                <span class="title-tag">Services</span>
                <h2>What We Do</h2>
            </div>
            <div class="services-loop">
                <div class="row justify-content-center">
                    <div class="col-lg-4 col-md-6 col-sm-10">
                        <div class="single-service text-center white-bg">
                            <div class="icon">
                                <img src="{{ asset('assets/img/services/icon-1.png') }}" alt="Icon">
                            </div>
                            <h4>Website Development</h4>
                            <p>A website is your online identity. It should be made well to improve your online
                                identity. Get your website in industry-leading language or technologies that include
                                Laravel, React, Vuejs, Nodejs, WordPress, and more </p>
                            <a href="single-service.html" class="service-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-10">
                        <div class="single-service text-center secondary-bg doted mt-60">
                            <div class="icon">
                                <img src="{{ asset('assets/img/services/icon-2.png') }}" alt="Icon">
                            </div>
                            <h4>Mobile App Development</h4>
                            <p>Get mobile-friendly apps as per your business needs to stay ahead of the curve. We are
                                here to offer you the best Android, iOS, or Hybrid apps to make your business and
                                interaction with customers easy. </p>
                            <a href="single-service.html" class="service-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-10">
                        <div class="single-service text-center primary-bg mt-120">
                            <div class="icon">
                                <img src="{{ asset('assets/img/services/icon-3.png') }}" alt="Icon">
                            </div>
                            <h4>E-commerce</h4>
                            <p>Want to sell your products to millions across the globe? E-commerce is the answer for
                                you! Time to scale up your e-commerce business for increasing your profits to many folds
                                with our best business-driving strategies.</p>
                            <a href="single-service.html" class="service-link">Read More</a>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6 col-sm-10">
                        <div class="single-service text-center white-bg">
                            <div class="icon">
                                <img src="{{ asset('assets/img/services/icon-4.png') }}" alt="Icon">
                            </div>
                            <h4>Software Development</h4>
                            <p>Want to sell your products to millions across the globe? E-commerce is the answer for
                                you! Time to scale up your e-commerce business for increasing your profits to many folds
                                with our best business-driving strategies.</p>

                            <a href="single-service.html" class="service-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-10">
                        <div class="single-service text-center secondary-bg doted mt-60">
                            <div class="icon">
                                <img src="{{ asset('assets/img/services/icon-5.png') }}" alt="Icon">
                            </div>
                            <h4>UI/UX Design</h4>
                            <p>UI/UX consultation and development services to design software products that win the
                                hearts of your customers.We will draw your dreams digitally and according to your
                                expectations. </p>
                            <a href="single-service.html" class="service-link">Read More</a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-10">
                        <div class="single-service text-center primary-bg mt-120">
                            <div class="icon">
                                <img src="{{ asset('assets/img/services/icon-6.png') }}" alt="Icon">
                            </div>
                            <h4>Technical Support</h4>
                            <p>Get mobile-friendly apps as per your business needs to stay ahead of the curve. We are
                                here to offer you the best Android, iOS, or Hybrid apps to make your business and
                                interaction with customers easy. </p>
                            <a href="single-service.html" class="service-link">Read More</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--====== SERVICES SECTION END ======-->
    <!--====== FEATURE SECTION START ======-->
    <section class="features-boxes pt-120 pb-110">
        <div class="container">
            <!-- Section Title -->
            <div class="row align-items-center">
                <div class="col-lg-6 col-md-8 col-sm-7">
                    <div class="section-title left-border">
                        <span class="title-tag">Why Us</span>
                        <h2>Why Choose Us</h2>
                    </div>
                </div>
                <div class="col-lg-6 col-md-4 col-sm-5 d-none d-sm-block">
                    <div class="text-right">
                        <a href="#" class="main-btn btn-filled">lEARN MORE</a>
                    </div>
                </div>
            </div>
            <!-- Feature boxes loop -->
            <div class="features-boxes-loop mt-100">
                <div class="row justify-content-center justify-content-md-between">
                    <div class="col-lg-4 col-md-5 col-sm-10">
                        <div class="feature-box">
                            <h5 style="    margin-bottom: 10px;"><i class="fal fa-browser"></i> WE CAN HELP YOU VALIDATE
                                IDEAS.</h5>
                            <p>Our idea validation process is designed to determine the viability of every project so we
                                can deliver reliable and de-risked solutions.</p>
                        </div>
                        <div class="feature-box">
                            <h5 style="    margin-bottom: 10px;"><i class="fal fa-cog"></i> WE WILL HELP YOU DEVELOP
                                YOUR IDEAS.</h5>
                            <p>We help clients make a mark in their domains with custom software development services
                                that delight the target end-users.</p>
                        </div>
                        <div class="feature-box">
                            <h5 style="    margin-bottom: 10px;"><i class="fal fa-headphones-alt"></i> WE HAVE
                                UNPARALLELED PROJECT GOVERNANCE.</h5>
                            <p>WE HAVE UNPARALLELED PROJECT GOVERNANCE.</p>
                        </div>
                    </div>
                    <div class="gap-for-img"></div>
                    <div class="col-lg-4 col-md-5 col-sm-10">
                        <div class="feature-box">
                            <h5 style="    margin-bottom: 10px;"><i class="fal fa-desktop"></i> RESEARCH-DRIVEN
                                DEVELOPMENT STRATEGY.</h5>
                            <p>Our software development lifecycle is designed to validate, iterate, and test every step
                                to deliver a research-driven strategy that warrants longevity.</p>
                        </div>
                        <div class="feature-box">
                            <h5 style="    margin-bottom: 10px;"><i class="fal fa-globe"></i> INTEGRATING
                                DESIGN-THINKING METHODOLOGIES.</h5>
                            <p>All of our deliverables are strong advocates of human-centred design that lets your
                                customers have an emulsifying experience.</p>
                        </div>
                        <div class="feature-box">
                            <h5 style="    margin-bottom: 10px;"><i class="fal fa-lock"></i> TECHNICAL SUPPORT AND
                                MAINTENANCE.</h5>
                            <p>Reliability is the cornerstone of our ethos; it helps us remain true to all our
                                technology partners, ultimately making impactful digital solutions.</p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- feature-img -->
            <div class="feature-img">
                <img src="{{ asset('assets/img/feature-img.png') }}" alt="Image">
            </div>
        </div>
    </section>
    <!--====== FEATURE SECTION END ======-->

    <!--====== TEAM SECTION START ======-->
    {{--    <section class="team-section has-slider pt-120 pb-120">--}}
    {{--        <div class="container-fluid">--}}
    {{--            <div class="row team-loop team-slider-one">--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/01.jpg') }}" alt="Team-Image">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>Rosa D.William</h3>--}}
    {{--                            <span>Founder & CEO</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/02.jpg') }}" alt="Team-Image">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>hilixer b. browni</h3>--}}
    {{--                            <span>co-founder</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/azeem.jpg') }}" alt="AZEEM UL REHMAN">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>AZEEM UL REHMAN</h3>--}}
    {{--                            <span>LAMP Stack Senior Software Enginner</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/04.jpg') }}" alt="Team-Image">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>pokoloko k. kilix</h3>--}}
    {{--                            <span>consultant</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/05.jpg') }}" alt="Team-Image">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>pokoloko k. kilix</h3>--}}
    {{--                            <span>consultant</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/01.jpg') }}" alt="Team-Image">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>Rosalina D. William</h3>--}}
    {{--                            <span>Founder & CEO</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/02.jpg') }}" alt="Team-Image">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>hilixer b. browni</h3>--}}
    {{--                            <span>co-founder</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/03.jpg') }}" alt="Team-Image">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>pokoloko k. kilix</h3>--}}
    {{--                            <span>consultant</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-3">--}}
    {{--                    <div class="member-box">--}}
    {{--                        <div class="member-img">--}}
    {{--                            <img src="{{ asset('assets/img/team/04.jpg') }}" alt="Team-Image">--}}
    {{--                        </div>--}}
    {{--                        <div class="member-info">--}}
    {{--                            <h3>pokoloko k. kilix</h3>--}}
    {{--                            <span>consultant</span>--}}
    {{--                        </div>--}}
    {{--                        <a href="#" class="socail-trigger">+</a>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    <!--====== TEAM SECTION END ======-->
    {{--    <!--====== SKILLS SECTION START ======-->--}}
    {{--    <section class="skills-section pt-120 pb-120">--}}
    {{--        <div class="container">--}}
    {{--            <div class="row align-items-center justify-content-center">--}}
    {{--                <div class="col-lg-6 col-md-8 col-sm-10">--}}
    {{--                    <div class="skill-bars">--}}
    {{--                        <div class="section-title mb-60 left-border">--}}
    {{--                            <span class="title-tag">skillset</span>--}}
    {{--                            <h2> Check Skillset & Manupulation </h2>--}}
    {{--                        </div>--}}
    {{--                        <div class="skill-progress mb-45">--}}
    {{--                            <div class="title d-flex justify-content-between">--}}
    {{--                                <span>Consulting & Marketing</span>--}}
    {{--                                <span>72%</span>--}}
    {{--                            </div>--}}
    {{--                            <div class="progressbar-wrap">--}}
    {{--                                <div class="progressbar" data-width="72">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="skill-progress mb-45">--}}
    {{--                            <div class="title d-flex justify-content-between">--}}
    {{--                                <span>it solution & travelshooting</span>--}}
    {{--                                <span>81%</span>--}}
    {{--                            </div>--}}
    {{--                            <div class="progressbar-wrap">--}}
    {{--                                <div class="progressbar" data-width="81">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                        <div class="skill-progress">--}}
    {{--                            <div class="title d-flex justify-content-between">--}}
    {{--                                <span>uix solution</span>--}}
    {{--                                <span>72%</span>--}}
    {{--                            </div>--}}
    {{--                            <div class="progressbar-wrap">--}}
    {{--                                <div class="progressbar" data-width="45">--}}
    {{--                                </div>--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="col-lg-6 col-md-8 col-sm-10">--}}
    {{--                    <div class="skill-img text-right">--}}
    {{--                        <img src="{{ asset('assets/img/skill-img.jpg') }}" alt="Image">--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <a href="#" class="main-btn btn-filled hire-btn">Hire Us Now</a>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </section>--}}
    {{--    <!--====== SKILLS SECTION END ======-->--}}
    <!--====== PORTFOLIO START ======-->
    <section class="portfolio-section pt-120 pb-90" id="portfolio">
        <div class="container">
            <div class="section-title text-center both-border mb-80">
                <span class="title-tag">portfolio</span>
                <h2>case study</h2>
            </div>
            <!-- portfolio loop -->
            <div class="row portfolio-masonary-loop">
                {{--                <div class="col-lg-4 col-sm-6">--}}
                {{--                    <div class="portfolio-box height-extra">--}}
                {{--                        <div class="portfolio-img"--}}
                {{--                             style="background-image: url({{ asset('assets/img/portfolio/1.jpg') }});"></div>--}}
                {{--                        <div class="portfolio-desc">--}}
                {{--                            <span class="portfolio-cat">Servie</span>--}}
                {{--                            <h4><a href="http://jaelynyan.com/" target="_blank">Restaurant Management System</a></h4>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                <div class="col-lg-6 col-sm-6">
                    <div class="portfolio-box height-extra"
                         style="background-image: url({{ asset('assets/img/portfolio/2A.jpg') }});">
                        <div class="portfolio-desc">
                            <span class="portfolio-cat">SolarNest</span>
                            <h4><a href="https://solarnest.pk/" target="_blank">Solar Panel System</a></h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div class="portfolio-box height-extra"
                         style="background-image: url({{ asset('assets/img/portfolio/3.jpg') }});">
                        <div class="portfolio-desc">
                            <span class="portfolio-cat">MimoFinds</span>
                            <h4><a href="https://mimofinds.ch/" target="_blank">Messaging System</a></h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <div class="portfolio-box height-extra"
                         style="background-image: url({{ asset('assets/img/portfolio/4.jpg') }});">
                        <div class="portfolio-desc">
                            <span class="portfolio-cat">Qalbish</span>
                            <h4><a href="https://qalbish.pk/" target="_blank">Salon Appointment Booking</a></h4>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-sm-12">
                    <div class="portfolio-box height-extra"
                         style="background-image: url({{ asset('assets/img/portfolio/5.jpg') }});">
                        <div class="portfolio-desc">
                            <span class="portfolio-cat">SafeChex</span>
                            <h4><a href="http://safechex.com/" target="_blank">Appointment Booking For Real State</a>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--====== PORTFOLIO END ======-->
    <!--====== TESTIMONIAL START ======-->
    @include('frontend.partials.testimonials')
    <!--====== TESTIMONIAL END ======-->
@endsection
