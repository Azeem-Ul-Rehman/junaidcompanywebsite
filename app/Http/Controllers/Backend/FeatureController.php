<?php

namespace App\Http\Controllers\Backend;

use App\Feature;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class FeatureController extends Controller
{


    public function index()
    {
        $features = Feature::orderBy('id', 'ASC')->get();
        return view('backend.features.index', compact('features'));
    }

    public function create()
    {
        return view('backend.features.create');
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'value' => 'required',

        ], [
            'title.required' => 'Title is required.',
            'value.required' => 'Value is required.',
        ]);


        $feature = Feature::create([
            'title' => $request->title,
            'value' => $request->value,
        ]);

        return redirect()->route('admin.features.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Feature created successfully.'
            ]);
    }

    public function edit($id)
    {
        $feature = Feature::find($id);
        return view('backend.features.edit', compact('feature'));


    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'value' => 'required',

        ], [
            'title.required' => 'Title is required.',
            'value.required' => 'Value is required.',
        ]);

        $feature = Feature::find($id);
        $feature->update([
            'title' => $request->title,
            'value' => $request->value,
        ]);

        return redirect()->route('admin.features.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Feature updated successfully.'
            ]);
    }

    public function destroy($id)
    {
        $feature = Feature::findOrFail($id);
        $feature->delete();

        return redirect()->route('admin.features.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Feature has been deleted'
            ]);
    }


}
