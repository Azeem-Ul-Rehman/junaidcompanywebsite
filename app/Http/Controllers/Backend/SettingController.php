<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $setting = Setting::first();
        return view('backend.settings.index', compact('setting'));
    }


    public function update(Request $request, $id)
    {
        $setting = Setting::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/logo');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $logo = $name;
        } else {

            $logo = $setting->logo;
        }
        if ($request->has('footer_logo')) {
            $image = $request->file('footer_logo');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/banner');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $footer_logo = $name;
        } else {
            $footer_logo = $setting->footer_logo;
        }

        $setting->update([
            'status' => $request->status,
            'logo' => $logo,
            'banner' => $footer_logo
        ]);
        return redirect()->route('admin.settings.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Setting updated successfully.'
            ]);

    }
}
