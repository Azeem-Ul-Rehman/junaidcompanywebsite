<?php

namespace App\Http\Controllers\Backend;

use App\Gallery;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function __construct()
    {
    }

    public function index()
    {

        $galleries = Gallery::all();
        return view('backend.gallery.view', compact('galleries'));
    }

    public function create()
    {

        return view('backend.gallery.create');

    }

    public function store(Request $request)
    {

        ini_set('upload_max_filesize', '100M');
        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images/gallery'), $imageName);

        $imageUpload = new Gallery();
        $imageUpload->image = $imageName;
        $imageUpload->save();

        if ($image) {
            return response()->json($image, 200);
        } // Else, return error 400
        else {
            return response()->json('error', 400);
        }
    }

    public function destroy($id)
    {
        $gallery = Gallery::findOrFail($id);
        $filename = $gallery->image;
        Gallery::where('image', $filename)->delete();
        $path = public_path() . '/images/gallery/' . $filename;
        if (file_exists($path)) {
            unlink($path);
        }

        $gallery->delete();
        return redirect()->route('admin.galleries.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Gallery Image has been deleted'
            ]);
    }

    public function destroyGallery(Request $request)
    {
        $filename = $request->get('filename');
        Gallery::where('image', $filename)->delete();
        $path = public_path() . '/images/gallery/' . $filename;
        if (file_exists($path)) {
            unlink($path);
        }
        return $filename;
    }


}
