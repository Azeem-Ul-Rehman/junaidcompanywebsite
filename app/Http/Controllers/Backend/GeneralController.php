<?php

namespace App\Http\Controllers\Backend;

use App\General;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class GeneralController extends Controller
{
    public function index()
    {
        $general = General::first();
        return view('backend.general.create', compact('general'));
    }

    public function store(Request $request)
    {


        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'shipping_details' => 'required',
            'payment_details' => 'required',
            'return_policy' => 'required',
            'additional_information' => 'required',
        ], [
            'title.required' => 'Title is required.',
            'description.required' => 'Description is required.',
            'shipping_details.required' => 'Shipping Detail is required.',
            'payment_details.required' => 'Payment Details is required.',
            'return_policy.required' => 'Return Policy is required.',
            'additional_information.required' => 'Additional Information is required.'
        ]);
        $general = General::first();
        if (!is_null($general)) {
            $general->update([
                'title' => $request->title,
                'description' => $request->description,
                'shipping_details' => $request->shipping_details,
                'payment_details' => $request->payment_details,
                'return_policy' => $request->return_policy,
                'additional_information' => $request->additional_information,
            ]);
        } else {
            $general = new General();
            $general->title = $request->title;
            $general->description = $request->description;
            $general->shipping_details = $request->shipping_details;
            $general->payment_details = $request->payment_details;
            $general->return_policy = $request->return_policy;
            $general->additional_information = $request->additional_information;
            $general->save();
        }
        return back()->with([
            'flash_status' => 'success',
            'flash_message' => 'Information updated successfully.'
        ]);
    }

}
