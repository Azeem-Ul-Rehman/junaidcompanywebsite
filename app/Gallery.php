<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = "galleries";
    protected $guarded = [];

    public function getImagePathAttribute()
    {
        return asset('images/gallery/' . $this->image);
    }
}
