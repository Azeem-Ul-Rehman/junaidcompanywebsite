<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/cache', function () {

    \Illuminate\Support\Facades\Artisan::call('key:generate');
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('config:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');
    \Illuminate\Support\Facades\Artisan::call('route:clear');

    return 'Commands run successfully Cleared.';
});
Route::get('/', function () {

    return view('welcome');

})->name('index');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about-us', 'FrontendController@about')->name('about');
Route::get('/services', 'FrontendController@services')->name('services');
Route::get('/contact-us', 'FrontendController@contactUs')->name('contact-us');

Route::get('forget-password', 'AccountController@getEmail');
Route::post('forget-password', 'AccountController@postEmail');
Route::get('reset-password/{token}', 'AccountController@getPassword');
Route::post('reset-password', 'AccountController@updatePassword');


Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('/galleries', 'Backend\GalleryController');
        Route::post('single/gallery/delete', 'Backend\GalleryController@destroyGallery')->name('single.gallery.destroy');
        Route::resource('/features', 'Backend\FeatureController');
        Route::resource('/general', 'Backend\GeneralController', [
            'only' => ['index', 'store']
        ]);
        Route::resource('/settings', 'Backend\SettingController', [
            'only' => ['index', 'update']
        ]);
    });
});


